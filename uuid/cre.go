package uuid

import "github.com/rs/xid"

func NewUUid() string {
	return xid.New().String()
}
